/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package common

import (
	"bytes"
	"sync"

	"chainmaker.org/chainmaker/protocol/v2"
)

// PeerIdTlsCertStore record the tls cert bytes of peer .
type PeerIdTlsCertStore struct {
	logger protocol.Logger
	lock   sync.RWMutex
	store  map[string][]byte
}

func NewPeerIdTlsCertStore(logger protocol.Logger) *PeerIdTlsCertStore {
	return &PeerIdTlsCertStore{store: make(map[string][]byte), logger: logger}
}

func (p *PeerIdTlsCertStore) SetPeerTlsCert(peerId string, tlsCert []byte) {
	p.lock.Lock()
	defer p.lock.Unlock()
	c, ok := p.store[peerId]
	if ok {
		if bytes.Compare(c, tlsCert) != 0 {
			p.store[peerId] = tlsCert
		}
	} else {
		p.store[peerId] = tlsCert
	}
}

func (p *PeerIdTlsCertStore) RemoveByPeerId(peerId string) {
	p.lock.Lock()
	defer p.lock.Unlock()
	if _, ok := p.store[peerId]; ok {
		delete(p.store, peerId)
	}
}

func (p *PeerIdTlsCertStore) GetCertByPeerId(peerId string) []byte {
	p.lock.RLock()
	defer p.lock.RUnlock()
	if cert, ok := p.store[peerId]; ok {
		return cert
	}
	return nil
}

func (p *PeerIdTlsCertStore) StoreCopy() map[string][]byte {
	p.lock.RLock()
	defer p.lock.RUnlock()
	newMap := make(map[string][]byte)
	for pid := range p.store {
		temp := p.store[pid]
		newBytes := make([]byte, len(temp))
		copy(newBytes, temp)
		newMap[pid] = newBytes
	}
	return newMap
}
