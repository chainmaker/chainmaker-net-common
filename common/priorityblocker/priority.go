/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package priorityblocker

import "sync"

// Priority for blocking with blocker.
type Priority uint8

const (
	PriorityLevel0 Priority = iota
	PriorityLevel1
	PriorityLevel2
	PriorityLevel3
	PriorityLevel4
	PriorityLevel5
	PriorityLevel6
	PriorityLevel7
	PriorityLevel8
	PriorityLevel9
)

// flagPriorityRecorder record the priority of flags.
type flagPriorityRecorder struct {
	m sync.Map
}

// SetPriority set the priority of the flag.
func (r *flagPriorityRecorder) SetPriority(flag string, p Priority) {
	r.m.LoadOrStore(flag, p)
}

// GetPriority query the priority of the flag.
// If it has not been set, default return PriorityLevel5.
func (r *flagPriorityRecorder) GetPriority(flag string) Priority {
	v, ok := r.m.Load(flag)
	if ok {
		return v.(Priority)
	}
	return PriorityLevel5
}
