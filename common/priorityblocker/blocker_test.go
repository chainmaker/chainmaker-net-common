/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package priorityblocker

import (
	"github.com/stretchr/testify/require"
	"sync"
	"testing"
)

func TestBlockerBlock(t *testing.T) {
	A, B, C, D := "A", "B", "C", "D"

	blocker := NewBlocker(nil)
	blocker.SetPriority(A, PriorityLevel9)
	blocker.SetPriority(B, PriorityLevel7)
	blocker.SetPriority(C, PriorityLevel5)
	blocker.SetPriority(D, PriorityLevel3)

	aC := make(chan struct{})
	bC := make(chan struct{})
	cC := make(chan struct{})
	dC := make(chan struct{})

	blockTimes := 100000
	var wgA, wgB, wgC, wgD sync.WaitGroup
	wgA.Add(blockTimes)
	wgB.Add(blockTimes)
	wgC.Add(blockTimes)
	wgD.Add(blockTimes)
	for i := 0; i < blockTimes; i++ {
		go func() {
			blocker.Block(A)
			wgA.Done()
		}()
		go func() {
			blocker.Block(B)
			wgB.Done()
		}()
		go func() {
			blocker.Block(C)
			wgC.Done()
		}()
		go func() {
			blocker.Block(D)
			wgD.Done()
		}()
		if i == blockTimes/2 {
			blocker.Run()
		}
	}

	go func() {
		wgA.Wait()
		aC <- struct{}{}
	}()
	go func() {
		wgB.Wait()
		bC <- struct{}{}
	}()
	go func() {
		wgC.Wait()
		cC <- struct{}{}
	}()
	go func() {
		wgD.Wait()
		dC <- struct{}{}
	}()

	res := [4]int{}
	for i := 0; i < 4; i++ {
		select {
		case <-aC:
			res[i] = 1
		case <-bC:
			res[i] = 2
		case <-cC:
			res[i] = 3
		case <-dC:
			res[i] = 4
		}
	}
	//fmt.Printf("%v\n", res)
	require.True(t, res[0] == 1)
	require.True(t, res[1] == 2)
	require.True(t, res[2] == 3)
	require.True(t, res[3] == 4)
}

func TestBlockerBlockWithOuterTicketsPrinter(t *testing.T) {
	A, B, C, D := "A", "B", "C", "D"
	ticketC := make(chan struct{})
	blocker := NewBlocker(ticketC)
	blocker.SetPriority(A, PriorityLevel9)
	blocker.SetPriority(B, PriorityLevel7)
	blocker.SetPriority(C, PriorityLevel5)
	blocker.SetPriority(D, PriorityLevel3)

	aC := make(chan struct{})
	bC := make(chan struct{})
	cC := make(chan struct{})
	dC := make(chan struct{})

	blockTimes := 100000
	var wgA, wgB, wgC, wgD sync.WaitGroup
	wgA.Add(blockTimes)
	wgB.Add(blockTimes)
	wgC.Add(blockTimes)
	wgD.Add(blockTimes)
	blocker.Run()
	for i := 0; i < blockTimes; i++ {
		go func() {
			blocker.Block(A)
			wgA.Done()
		}()
		go func() {
			blocker.Block(B)
			wgB.Done()
		}()
		go func() {
			blocker.Block(C)
			wgC.Done()
		}()
		go func() {
			blocker.Block(D)
			wgD.Done()
		}()
		if i == blockTimes/2 {
			go func() {
				for {
					ticketC <- struct{}{}
				}
			}()
		}
	}

	go func() {
		wgA.Wait()
		aC <- struct{}{}
	}()
	go func() {
		wgB.Wait()
		bC <- struct{}{}
	}()
	go func() {
		wgC.Wait()
		cC <- struct{}{}
	}()
	go func() {
		wgD.Wait()
		dC <- struct{}{}
	}()

	res := [4]int{}
	for i := 0; i < 4; i++ {
		select {
		case <-aC:
			res[i] = 1
		case <-bC:
			res[i] = 2
		case <-cC:
			res[i] = 3
		case <-dC:
			res[i] = 4
		}
	}
	//fmt.Printf("%v\n", res)
	require.True(t, res[0] == 1)
	require.True(t, res[1] == 2)
	require.True(t, res[2] == 3)
	require.True(t, res[3] == 4)

}
