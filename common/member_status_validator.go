/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package common

import (
	"sync"

	pbac "chainmaker.org/chainmaker/pb-go/v2/accesscontrol"
	"chainmaker.org/chainmaker/protocol/v2"
)

// MemberStatusValidator is a validator for validating member status.
type MemberStatusValidator struct {
	accessControls sync.Map
	blockedPeerIds sync.Map
}

// NewMemberStatusValidator create a new MemberStatusValidator instance.
func NewMemberStatusValidator() *MemberStatusValidator {
	return &MemberStatusValidator{}
}

// AddPeerId Add a pid to blocked list.
func (v *MemberStatusValidator) AddPeerId(pid string) {
	v.blockedPeerIds.LoadOrStore(pid, struct{}{})
}

// RemovePeerId remove pid given from blocked list.
func (v *MemberStatusValidator) RemovePeerId(pid string) {
	v.blockedPeerIds.Delete(pid)
}

// ContainsPeerId return whether pid given exist in blocked list.
func (v *MemberStatusValidator) ContainsPeerId(pid string) bool {
	_, ok := v.blockedPeerIds.Load(pid)
	return ok
}

// AddAC Add access control of chain to validator.
func (v *MemberStatusValidator) AddAC(chainId string, ac protocol.AccessControlProvider) {
	v.accessControls.LoadOrStore(chainId, ac)
}

// ValidateMemberStatus check the status of members.
func (v *MemberStatusValidator) ValidateMemberStatus(members []*pbac.Member) (bool, error) {
	bl := true
	var err error
	v.accessControls.Range(func(key, value interface{}) bool {
		ac, _ := value.(protocol.AccessControlProvider)
		if ac == nil {
			return false
		}
		allOk := true
		for _, member := range members {
			var s pbac.MemberStatus
			s, err = ac.GetMemberStatus(member)
			if err != nil {
				return false
			}
			if s == pbac.MemberStatus_INVALID || s == pbac.MemberStatus_FROZEN || s == pbac.MemberStatus_REVOKED {
				allOk = false
				break
			}
		}
		if allOk {
			bl = true
			return false
		}
		return true
	})
	if err != nil {
		return false, err
	}
	return bl, nil
}
