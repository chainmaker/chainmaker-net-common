/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package common

import cmx509 "chainmaker.org/chainmaker/common/v2/crypto/x509"

func ParsePeerIdCertBytesMapToPeerIdCertMap(peerIdCertBytesMap map[string][]byte) (map[string]*cmx509.Certificate, error) {
	peerIdCertMap := make(map[string]*cmx509.Certificate)
	for pid := range peerIdCertBytesMap {
		certBytes := peerIdCertBytesMap[pid]
		cert, err := cmx509.ParseCertificate(certBytes)
		if err != nil {
			return nil, err
		}
		peerIdCertMap[pid] = cert
	}
	return peerIdCertMap, nil
}
