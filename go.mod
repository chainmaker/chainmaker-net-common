module chainmaker.org/chainmaker/chainmaker-net-common

go 1.15

require (
	chainmaker.org/chainmaker/common/v2 v2.0.1-0.20210906085649-78f6202d8d60
	chainmaker.org/chainmaker/pb-go/v2 v2.0.0
	chainmaker.org/chainmaker/protocol/v2 v2.0.1-0.20210906092203-47d66f4908f7
	github.com/libp2p/go-libp2p-core v0.6.1
	github.com/multiformats/go-multiaddr v0.3.1
	github.com/stretchr/testify v1.7.0
	github.com/tjfoc/gmsm v1.4.1
	github.com/xiaotianfork/q-tls-common v0.0.10
	github.com/xiaotianfork/quic-go v0.21.20
	golang.org/x/tools v0.1.4 // indirect
)

replace github.com/libp2p/go-libp2p-core => chainmaker.org/chainmaker/libp2p-core v0.0.2
